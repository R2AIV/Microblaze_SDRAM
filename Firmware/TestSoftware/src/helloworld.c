#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "platform.h"

#include "xparameters.h"
#include "xgpio.h"

void print(char *str);

void delay()
{
	uint32_t i,j;
	for(i=0;i<1000;i++)
		for(j=0;j<50;j++);
}

XGpio MyGPIO;

int main()
{
    init_platform();

    XGpio_Initialize(&MyGPIO, XPAR_XPS_GPIO_0_DEVICE_ID);
    XGpio_SetDataDirection(&MyGPIO,1,0x00);

    while(1)
    {
    	XGpio_DiscreteWrite(&MyGPIO,1,0xAA);
    	delay();
    	XGpio_DiscreteWrite(&MyGPIO,1,0x99);
    	delay();
    }

    return 0;
}
