@ECHO OFF
@REM ###########################################
@REM # Script file to run the flow 
@REM # 
@REM ###########################################
@REM #
@REM # Command line for ngdbuild
@REM #
ngdbuild -p xc3s500epq208-4 -nt timestamp -bm it_works.bmm "F:/Microblaze_SDRAM/implementation/it_works.ngc" -uc it_works.ucf it_works.ngd 

@REM #
@REM # Command line for map
@REM #
map -o it_works_map.ncd -pr b -ol high -timing -detail it_works.ngd it_works.pcf 

@REM #
@REM # Command line for par
@REM #
par -w -ol high it_works_map.ncd it_works.ncd it_works.pcf 

@REM #
@REM # Command line for post_par_trce
@REM #
trce -e 3 -xml it_works.twx it_works.ncd it_works.pcf 

