-------------------------------------------------------------------------------
-- it_works_stub.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity it_works_stub is
  port (
    fpga_0_clk_1_sys_clk_pin : in std_logic;
    fpga_0_rst_1_sys_rst_pin : in std_logic;
    mpmc_0_SDRAM_Clk_pin : out std_logic;
    mpmc_0_SDRAM_CE_pin : out std_logic;
    mpmc_0_SDRAM_CS_n_pin : out std_logic;
    mpmc_0_SDRAM_RAS_n_pin : out std_logic;
    mpmc_0_SDRAM_CAS_n_pin : out std_logic;
    mpmc_0_SDRAM_WE_n_pin : out std_logic;
    mpmc_0_SDRAM_BankAddr_pin : out std_logic_vector(1 downto 0);
    mpmc_0_SDRAM_Addr_pin : out std_logic_vector(12 downto 0);
    mpmc_0_SDRAM_DQ : inout std_logic_vector(15 downto 0);
    mpmc_0_SDRAM_DM_pin : out std_logic_vector(1 downto 0);
    xps_gpio_0_GPIO_IO_pin : inout std_logic_vector(0 to 7)
  );
end it_works_stub;

architecture STRUCTURE of it_works_stub is

  component it_works is
    port (
      fpga_0_clk_1_sys_clk_pin : in std_logic;
      fpga_0_rst_1_sys_rst_pin : in std_logic;
      mpmc_0_SDRAM_Clk_pin : out std_logic;
      mpmc_0_SDRAM_CE_pin : out std_logic;
      mpmc_0_SDRAM_CS_n_pin : out std_logic;
      mpmc_0_SDRAM_RAS_n_pin : out std_logic;
      mpmc_0_SDRAM_CAS_n_pin : out std_logic;
      mpmc_0_SDRAM_WE_n_pin : out std_logic;
      mpmc_0_SDRAM_BankAddr_pin : out std_logic_vector(1 downto 0);
      mpmc_0_SDRAM_Addr_pin : out std_logic_vector(12 downto 0);
      mpmc_0_SDRAM_DQ : inout std_logic_vector(15 downto 0);
      mpmc_0_SDRAM_DM_pin : out std_logic_vector(1 downto 0);
      xps_gpio_0_GPIO_IO_pin : inout std_logic_vector(0 to 7)
    );
  end component;

  attribute BOX_TYPE : STRING;
  attribute BOX_TYPE of it_works : component is "user_black_box";

begin

  it_works_i : it_works
    port map (
      fpga_0_clk_1_sys_clk_pin => fpga_0_clk_1_sys_clk_pin,
      fpga_0_rst_1_sys_rst_pin => fpga_0_rst_1_sys_rst_pin,
      mpmc_0_SDRAM_Clk_pin => mpmc_0_SDRAM_Clk_pin,
      mpmc_0_SDRAM_CE_pin => mpmc_0_SDRAM_CE_pin,
      mpmc_0_SDRAM_CS_n_pin => mpmc_0_SDRAM_CS_n_pin,
      mpmc_0_SDRAM_RAS_n_pin => mpmc_0_SDRAM_RAS_n_pin,
      mpmc_0_SDRAM_CAS_n_pin => mpmc_0_SDRAM_CAS_n_pin,
      mpmc_0_SDRAM_WE_n_pin => mpmc_0_SDRAM_WE_n_pin,
      mpmc_0_SDRAM_BankAddr_pin => mpmc_0_SDRAM_BankAddr_pin,
      mpmc_0_SDRAM_Addr_pin => mpmc_0_SDRAM_Addr_pin,
      mpmc_0_SDRAM_DQ => mpmc_0_SDRAM_DQ,
      mpmc_0_SDRAM_DM_pin => mpmc_0_SDRAM_DM_pin,
      xps_gpio_0_GPIO_IO_pin => xps_gpio_0_GPIO_IO_pin
    );

end architecture STRUCTURE;

